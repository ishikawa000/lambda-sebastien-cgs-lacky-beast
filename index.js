'use strict'

exports.handler = (event, context, callback) => {
    callback(null, {
        'error_code': 'success',
        'status': true,
        'bot_id': event.bot_id,
        'user_id': event.user_id,
        'params': {
            'status': true,
            'talkend': true,
            'message': '＜ ' + event.args.utterance
        }
    })
}
